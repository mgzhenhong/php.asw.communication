<?php

namespace Protocols;

use ASW\Communication\Rdc\RdcClientRequest;
use ASW\Communication\Rdc\RdcServerFrame;

class Rdcs
{
    /**
     * 检查包的完整性
     * 如果能够得到包长，则返回包的在 buffer 中的长度，否则返回 0 继续等待数据
     * 如果协议有问题，则可以返回 false ，当前客户端连接会因此断开
     */
    public static function input(string $buffer): int
    {
        if (strlen($buffer) < 4) return 0;
        $unpack = unpack('NtotalLength', $buffer);
        return $unpack['totalLength'];
    }

    /**
     * 打包，当向服务端发送数据的时候会自动调用
     */
    public static function encode(RdcServerFrame $frame): string
    {
        $json        = json_encode($frame, JSON_UNESCAPED_UNICODE);
        $totalLength = 4 + strlen($json);
        return pack('N', $totalLength) . $json;
    }

    /**
     * 解包，当接收到的数据字节数等于input返回的值（大于0的值）自动调用
     * 并传递给onMessage回调函数的$data参数
     */
    public static function decode(string $buffer): RdcClientRequest
    {
        $json  = substr($buffer, 4);
        $array = json_decode($json, true);
        return RdcClientRequest::fromArray($array);
    }
}
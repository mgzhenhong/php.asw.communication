<?php

namespace ASW\Communication\Rdc;


use ASW\Utility\ExecuteResult;

class RdcServerResponse extends RdcServerFrame
{

    public ExecuteResult $result;

    private function __construct()
    {
    }

    public static function fromRequest(RdcClientRequest $request, ExecuteResult $result): RdcServerResponse
    {
        $instance         = new static();
        $instance->seq    = $request->seq;
        $instance->type   = RdcServerFrameType::Response;
        $instance->result = $result;
        return $instance;
    }

    public static function fromArray(array $array): static
    {
        $instance         = new static();
        $instance->type   = RdcServerFrameType::Response;
        $instance->seq    = $array['seq'] ?? 0;
        $instance->result = ExecuteResult::fromArray($array['result'] ?? []);
        return $instance;
    }
}
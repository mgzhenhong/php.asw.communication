<?php

namespace ASW\Communication\Rdc;

enum RdcClientRequestBehavior : string
{
    /**
     * 向服务端声明无需返回响应
     */
    case NO_RESPONSE = 'noResponse';

    /**
     * 向服务端声明处理后关闭连接
     */
    case CLOSE_ME = 'closeMe';
}
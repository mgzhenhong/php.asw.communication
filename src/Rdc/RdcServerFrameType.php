<?php

namespace ASW\Communication\Rdc;


enum RdcServerFrameType: string
{
    case Command = 'command';

    case Response = 'response';
}
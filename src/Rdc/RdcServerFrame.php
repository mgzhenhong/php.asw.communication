<?php

namespace ASW\Communication\Rdc;


class RdcServerFrame extends RdcFrame
{
    public RdcServerFrameType $type;

    public static function fromArray(array $array): ?static
    {
        $type = $array['type'] ?? '';
        return match ($type) {
            RdcServerFrameType::Response->value => RdcServerResponse::fromArray($array),
            RdcServerFrameType::Command->value => RdcServerCommand::fromArray($array),
            default => null,
        };
    }
}
<?php
/**
 * Product: ASW.Communication.
 * Date: 2024-05-16
 * Time: 12:12
 */

require '../vendor/autoload.php';

use ASW\Communication\EventChannelClient;

function writeLog(string $content): void
{
    $timeStr = date('Y-m-d H:i:s');
    echo "$timeStr $content" . PHP_EOL;
}

$publishTimer = 0;
$worker = new \Workerman\Worker();
$worker->onWorkerStart = function () {

    // 创建客户端
    $eventChannelClient = new EventChannelClient();

    // 订阅事件
    $eventChannelClient->on('test', function (string $eventName, array $eventArgs) {
        writeLog("received event [$eventName] [" . json_encode($eventArgs) . "]");
    });

    $eventChannelClient->onConnected(function (EventChannelClient $eventChannelClient) {
        writeLog("client connected");
    });

    $eventChannelClient->onClosed(function (EventChannelClient $eventChannelClient) {
        writeLog("client closed");
    });

    $eventChannelClient->connect();
};

\Workerman\Worker::runAll();
<?php
/**
 * Product: ASW.Communication.
 * Date: 2024-05-16
 * Time: 10:16
 */

require '../vendor/autoload.php';

use ASW\Communication\EventChannelServer;
use ASW\Communication\Rdc\RdcClientRequest;
use ASW\Communication\Rdc\RdcServerCommand;
use ASW\Communication\Rdc\RdcServerResponse;
use Workerman\Connection\TcpConnection;

function writeLog(string $content): void
{
    $timeStr = date('Y-m-d H:i:s');
    echo "$timeStr $content" . PHP_EOL;
}

// 创建并启动服务端
$eventChannelServer = new EventChannelServer();
$eventChannelServer->start();

$eventChannelServer->onServerStart = function () {
    writeLog("server started");
};

// 客户端发送缓冲区满
$eventChannelServer->onClientSendBufferFull = function (TcpConnection $connection) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client send buffer full [$connectionId] [$remoteAddress]");
};

// 客户端发送缓冲区空
$eventChannelServer->onClientSendBufferDrain = function (TcpConnection $connection) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client send buffer drain [$connectionId] [$remoteAddress]");
};

// 客户端连接
$eventChannelServer->onClientConnect = function (TcpConnection $connection) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client connected [$connectionId] [$remoteAddress]");
};

// 客户端断开连接
$eventChannelServer->onClientClose = function (TcpConnection $connection) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client closed [$connectionId] [$remoteAddress]");
};

// 客户端连接错误或通讯错误
$eventChannelServer->onClientError = function (TcpConnection $connection, int $code, string $msg) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client error [$connectionId] [$remoteAddress] [$code] [$msg]");
};

// 收到客户端发送请求
$eventChannelServer->onClientRequest = function (TcpConnection $connection, RdcClientRequest $request) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client request [$connectionId] [$remoteAddress] [seq: $request->seq] [action: $request->action]");
};

// 收到客户端订阅请求
$eventChannelServer->onClientSubscribe = function (TcpConnection $connection, string $eventName) use ($eventChannelServer) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    $subscriberCount = $eventChannelServer->getEventSubscriberCount($eventName);
    writeLog("client subscribed [$connectionId] [$remoteAddress] [$eventName], current count: [$subscriberCount]");
};

// 收到客户端取消订阅请求
$eventChannelServer->onClientUnSubscribe = function (TcpConnection $connection, string $eventName) use ($eventChannelServer) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    $subscriberCount = $eventChannelServer->getEventSubscriberCount($eventName);
    writeLog("client unsubscribed [$connectionId] [$remoteAddress] [$eventName], current count: [$subscriberCount]");
};

// 收到客户端发布事件请求
$eventChannelServer->onClientPublish = function (TcpConnection $connection, string $eventName, array $eventArgs) use ($eventChannelServer) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("client published [$connectionId] [$remoteAddress] [$eventName]");
};

// 服务端创建发布指令, 准备对其它客户端发布事件
$eventChannelServer->onServerPublishCommandCreate = function (RdcServerCommand $command) {
    // 可以修改 $command 内容
    $command->args['eventArgs']['server_append'] = 1;
};

// 服务端丢弃了应当对客户端发布的事件
$eventChannelServer->onClientEventDiscard = function (TcpConnection $connection, RdcServerCommand $command, string $reason) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("event discard [$connectionId] [$remoteAddress] [seq: $command->seq] [reason: $reason]");
};

// 服务端即将发送响应
$eventChannelServer->onBeforeSendResponse = function (TcpConnection $connection, RdcServerResponse $response) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("before send response [$connectionId] [$remoteAddress] [seq: $response->seq] [result: $response->result]");
};

// 服务端已经发送响应
$eventChannelServer->onAfterSendResponse = function (TcpConnection $connection, RdcServerResponse $response, bool|null $sendResult) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("after send response [$connectionId] [$remoteAddress] [seq: $response->seq] [result: $response->result] [result: $sendResult]");
};

// 服务端即将发送命令
$eventChannelServer->onBeforeSendCommand = function (TcpConnection $connection, RdcServerCommand $command) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("before send command [$connectionId] [$remoteAddress] [seq: $command->seq] [action: $command->action]");
};

// 服务端已经发送命令
$eventChannelServer->onAfterSendCommand = function (TcpConnection $connection, RdcServerCommand $command, bool|null $sendResult) {
    $connectionId = $connection->id;
    $remoteAddress = $connection->getRemoteAddress();
    writeLog("after send command [$connectionId] [$remoteAddress] [seq: $command->seq] [action: $command->action] [result: $sendResult]");
};

\Workerman\Worker::runAll();
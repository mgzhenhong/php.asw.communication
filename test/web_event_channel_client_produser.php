<?php
/**
 * Product: ASW.Communication.
 * Date: 2024-05-16
 * Time: 12:12
 */

require '../vendor/autoload.php';

use ASW\Communication\EventChannelClient;

function writeLog(string $content): void
{
    $timeStr = date('Y-m-d H:i:s');
    echo "$timeStr $content" . PHP_EOL;
}

$channelClient = new EventChannelClient();

$sendResult = $channelClient->publishSync('test', ['from' => 'web_channel_client', 'time' => time()]);
if (!$sendResult->result) {
    writeLog("publishSync 发布失败: $sendResult->info");
} else {
    writeLog("publishSync 发布成功");
}

$sendResult = $channelClient->publishSyncNoResponse('test', ['from' => 'web_channel_client', 'time' => time()]);
if (!$sendResult->result) {
    writeLog("publishSyncNoResponse 发布失败: $sendResult->info");
} else {
    writeLog("publishSyncNoResponse 发布成功");
}

